Repositories
============

All the relevant repositories and branches are described below. For the current
list of maintainers, mailing lists, etc. please refer to MAINTAINERS_.

.. _MAINTAINERS: https://gitlab.freedesktop.org/drm/tip/-/blob/drm-tip/MAINTAINERS

Contents:

.. toctree::
   :maxdepth: 2

   upstream
   drm-tip
   drm
   drm-misc
   drm-intel
   drm-xe
