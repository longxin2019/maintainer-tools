DRM Maintainer Tools
====================

This documentation covers the tools and workflows for maintaining and
contributing to the Linux kernel DRM subsystem's :ref:`drm`, :ref:`drm-misc`,
:ref:`drm-intel` and :ref:`drm-xe` repositories. The intended audience is
primarily the maintainers and committers of said repositories, but the workflow
documentation is useful for anyone interested in the kernel graphics subsystem
development.

The above mentioned repositories are maintained using the same tools and very
similar workflows. All feed to the same testing and integration tree, the
:ref:`drm-tip`. The documentation here is mostly shared, highlighting the
differences in workflows where applicable.

Please see :ref:`contributing` as well as the `project home page`_ for more
information on how to collaborate on the documentation and tools.

.. note::

   This documentation is an eternal draft and simply tries to explain the
   reality of how the DRM subsystem is maintained. If you observe a difference
   between these rules and reality, it is your assumed responsibility to update
   the rules.

.. _project home page: https://gitlab.freedesktop.org/drm/maintainer-tools/

Contents:

.. toctree::
   :maxdepth: 2

   Introduction <self>
   repositories/index
   committer/index
   maintainer/index
   dim man page <dim>
   code-of-conduct
   CONTRIBUTING
   MAINTAINERS
   COPYING

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
