.. _committer-guidelines:

====================
Committer Guidelines
====================

This document gathers together committer guidelines.

.. toctree::
   :maxdepth: 2

   commit-access
   getting-started
   committer-drm-misc
   committer-drm-intel
   conflict-resolution
   core-for-CI
